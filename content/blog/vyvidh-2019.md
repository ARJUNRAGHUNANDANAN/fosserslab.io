+++
title = "FOSSers At Vyvidh 2019"
image ="/img/vyvidh-2019/cover.jpg"
date = "2019-02-01T00:00:00+05:30"
tags = ["stall"]
categories = ["vyvidh"]
author = "Subin Siby"
aliases = ["/vyvidh19"]
+++

Vyvidh 2019 is here ! And FOSSers is conducting events !

After a long time, [FOSSers VAST](//fossers.vidyaacademy.ac.in) is conducting events for Vidya's tech-fest [Vyvidh](http://vyvidh.vidyaacademy.ac.in/) !

FOSSers has been in college for more than 10 years, but has been dormant. This year, we're excited to announce that we're back ! We're the only club to have special events for this year's tech fest !

## Stall

FOSSers/LinuxChix will have a stall at room **MB-109** (Middle block, first floor, room 109). Come visit us ! We're giving out swags !

* Know more about Free Software & Open Source Software
* Get to know how to contribute to open source softwares
* Want to install GNU/Linux in your lap ? Come on over !
* Free GNU/Linux support
* Learn how to be safe online & address your privacy concerns
* An open place for discussions !

Free As in Freedom !

## CrypTux

[CrypTux](http://cryptux.vidyaacademy.ac.in) is an online treasure hunt competition. Open from 2019 February 2 to 2019 February 8th.

Entry Fees : Free

Prize : Rs 1,000

Open to all students !

[Start Playing Now](http://cryptux.vidyaacademy.ac.in)

![](/img/vyvidh-2019/cryptux-poster.jpg)

## Pirate Tux

Pirate Tux is a unique exciting offline treasure hunt. We're using **Geocaching**, **Qr** codes and our very own software to implement this.

There are various levels of crypto puzzles, quizes etc. You can use the internet and any other resources to solve it. You'll also get to discover various part of the college you've never been into !

Get to know more about FOSS & our college.

Entry Fees : 10 per team member. Any number of members can be in the team

Prize : Rs 2,000

Special Prizes for participants ;)

## Wikigame

Find a way from one wiki page to a destination in the shortest time possible !

Entry : Free

Special Prizes

![](/img/vyvidh-2019/wikigame-poster.jpg)