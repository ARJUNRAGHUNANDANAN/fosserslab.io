+++
title = "Wikidata Workshop"
image = ""
date = "2019-02-26T00:00:00+05:30"
tags = ["stall"]
categories = ["events"]
author = "Subin Siby"
aliases = []
+++

## **[REGISTER HERE !](https://goo.gl/forms/qVh9LKMJFg0LfrCn2)**



FOSSers is conducting a Wikipedia + Wikidata workshop on March 4th, 2019. Wikidata is a free and open knowledge base that can be read and edited by both humans and machines.Wikidata is used for data science. It is a large public database of relations in Wikipedia, Wikivoyage, Wikisource and others.

Along with the workshop, we're planning to inaugurate LinuxChix VAST, a local community of the international LinuxChix community. LinuxChix is a community for women who like Linux and for anyone who wants to support women in computing : india.linuxchix.org. Our college would be the first to have a LinuxChix community. In India, LinuxChix is active only in New Delhi. We have also started a Kerala community to set up LinuxChix in more colleges in Kerala.

The workshop will have :

* Wikipedia content editing
* Using FOSS Tools for Wikidata
* Intro to SPARQL query language
* Using Python for accessing and interpreting the data on Wikidata

You'll learn how to contribute to Wikimedia projects :

* Editing Wikipedia articles
* Uploading your pictures to Wikimedia Commons
* Uploading data to Wikidata
* Using the data on Wikidata

Ranjith Siji, Kannan VM of [Wikimedians Kerala](https://meta.wikimedia.org/wiki/Wikimedians_of_Kerala) will handle the sessions.

This free workshop is sponsored by ICFOSS & Wikimedia Foundation.

> Event On : 2019 March 4th, 9AM
> 
> Event At : [Language Lab, Decennial Block (OpenStreetMap Link)](https://www.openstreetmap.org/node/6310148494)

[How to reach Vidya](/contact)

## **[REGISTER HERE !](https://goo.gl/forms/qVh9LKMJFg0LfrCn2)**

![Poster](/img/blog/wikidata-workshop-poster.png)